function getMyHousePower() {

  //Nature Cloud APIへのアクセストークンはGASのプロパティサービスに保存する
  let accessToken = PropertiesService.getScriptProperties().getProperty("accessToken");

  //Nature Cloud APIへのリクエストを送る時に付与するパラメータ
  let params = {
    "method" : "get",
    "headers" : {"Authorization":"Bearer " + accessToken}
  };

  //Nature Cloud APIのリクエストURL
  let requestUrl = 'https://api.nature.global/1/appliances';

  //Nature Remo Cloud APIへリクエスト送り取得したJSONデータを変数に格納
  let response = UrlFetchApp.fetch(requestUrl, params);
  let json = JSON.parse(response.getContentText());

  //Google Sheetのシート名を取得（Google Sheetのシート名がデフォルトの"シート1"の場合）
  let book = SpreadsheetApp.getActiveSpreadsheet();
  let sheet1Data = book.getSheetByName("シート1");
  let sheet2Data = book.getSheetByName("シート2");  //最新値用
  let sheet3Data = book.getSheetByName("1day_ago");  //1day_ago 用
  let sheet4Data = book.getSheetByName("2day_ago");  //2day_ago 用
  let sheet5Data = book.getSheetByName("1week_ago"); //1week_ago 用
  let sheet6Data = book.getSheetByName("1month_ago"); //1month_ago 用
  let sheet7Data = book.getSheetByName("1year_ago"); //1year_ago 用
  
  
  //Google Sheetの2行目に行を挿入する
  sheet1Data.insertRows(2,1);
  
  sheet3Data.insertRows(2,1);  //1day_ago 用
  sheet4Data.insertRows(2,1);  //2day_ago 用
  sheet5Data.insertRows(2,1);  //1week_ago 用
  sheet6Data.insertRows(2,1);  //1month_ago 用
  sheet7Data.insertRows(2,1);  //1year_ago 用

  
  //現在の日時を取得し2行目1列目のセルに日時を入力する
  let now = new Date();
  let val_time = Utilities.formatDate(now, 'Asia/Tokyo', 'yyyy/MM/dd HH:mm:ss');
  sheet1Data.getRange(2,1).setValue(val_time);
  sheet2Data.getRange(2,1).setValue(val_time);  // 最新値用
  sheet3Data.getRange(2,1).setValue(val_time);  //1day_ago 用
  sheet4Data.getRange(2,1).setValue(val_time);  //2day_ago 用
  sheet5Data.getRange(2,1).setValue(val_time);  //1week_ago 用
  sheet6Data.getRange(2,1).setValue(val_time);  //1month_ago 用
  sheet7Data.getRange(2,1).setValue(val_time);  //1year_ago 用

  
  // Googleデータポータル用に 日時（時単位）を2行目4列目のセルに入力する。
  let val_time3 = Utilities.formatDate(now, 'Asia/Tokyo', 'yyyyMMddHH');
  sheet1Data.getRange(2,4).setValue(val_time3);

  //1day_ago用に一日後の日付(時単位)を作る。
  let one_day_ago = new Date();
  one_day_ago.setHours(now.getHours() +24 );
  let val_time4 = Utilities.formatDate(one_day_ago, 'Asia/Tokyo', 'yyyyMMddHH');
  sheet3Data.getRange(2,4).setValue(val_time4);  // 1day_ago

  //2day_ago用に2日後の日付（時単位）を作る。
  let two_day_ago = new Date();
  two_day_ago.setHours(now.getHours() +24*2 );
  let val_time5 = Utilities.formatDate(two_day_ago, 'Asia/Tokyo', 'yyyyMMddHH');
  sheet4Data.getRange(2,4).setValue(val_time5);  // 2day_ago

  //1week_ago用に7日後の日付（時単位）を作る。
  let one_week_ago = new Date();
  one_week_ago.setHours(now.getHours() + 24*7 );
  let val_time6 = Utilities.formatDate(one_week_ago, 'Asia/Tokyo', 'yyyyMMddHH');
  sheet5Data.getRange(2,4).setValue(val_time6);  // 1week_ago  
  
  //1month_ago用に1月後の日付（時単位）を作る。
  let one_month_ago = new Date();
  one_month_ago.setMonth(now.getMonth() + 1 );
  let val_time7 = Utilities.formatDate(one_month_ago, 'Asia/Tokyo', 'yyyyMMddHH');
  sheet6Data.getRange(2,4).setValue(val_time7);  // 1month_ago  

  //1year_ago用に12月後の日付（時単位）を作る。
  let one_year_ago = new Date();
  one_year_ago.setMonth(now.getMonth() + 12 );
  let val_time8 = Utilities.formatDate(one_year_ago, 'Asia/Tokyo', 'yyyyMMddHH');
  sheet7Data.getRange(2,4).setValue(val_time8);  // 1month_ago  



  //APIから取得したJSON形式のデータから積算電力と瞬間電力の値を抜き出し、
  let val_power1 = 0 ;
  let val_power1_unit = 0 ;
  let val_power2 = 0 ;

  for(let i = 0 ; i < json[0]["smart_meter"]["echonetlite_properties"]["length"] ; i++ ){
    if(json[0]["smart_meter"]["echonetlite_properties"][i]["name"] == "normal_direction_cumulative_electric_energy" ){
      val_power1 = json[0]["smart_meter"]["echonetlite_properties"][i]["val"];
    }

    if(json[0]["smart_meter"]["echonetlite_properties"][i]["name"] == "cumulative_electric_energy_unit" ){
      val_power1_unit = json[0]["smart_meter"]["echonetlite_properties"][i]["val"];
    }

    if(json[0]["smart_meter"]["echonetlite_properties"][i]["name"] == "measured_instantaneous" ){
      val_power2 = json[0]["smart_meter"]["echonetlite_properties"][i]["val"];
    }
  }
  
  //積算電力の単位をkWhに統一して2行目2列目に入力
  sheet1Data.getRange(2, 2).setValue(val_power1 *( 0.1 ** val_power1_unit) );  
  sheet2Data.getRange(2, 2).setValue(val_power1 *( 0.1 ** val_power1_unit) ); // 最新値用
  sheet3Data.getRange(2, 2).setValue(val_power1 *( 0.1 ** val_power1_unit) ); // 1day_ago
  sheet4Data.getRange(2, 2).setValue(val_power1 *( 0.1 ** val_power1_unit) ); // 2day_ago
  sheet5Data.getRange(2, 2).setValue(val_power1 *( 0.1 ** val_power1_unit) ); // 1week_ago
  sheet6Data.getRange(2, 2).setValue(val_power1 *( 0.1 ** val_power1_unit) ); // 1month_ago
  sheet7Data.getRange(2, 2).setValue(val_power1 *( 0.1 ** val_power1_unit) ); // 1year_ago
  
  //瞬間電力の単位はWなのでそのまま2行目3列目に入力  
  sheet1Data.getRange(2, 3).setValue(val_power2);  
  sheet2Data.getRange(2, 3).setValue(val_power2); // 最新値用 
  sheet3Data.getRange(2, 3).setValue(val_power2); // 1day_ago 
  sheet4Data.getRange(2, 3).setValue(val_power2); // 2day_ago 
  sheet5Data.getRange(2, 3).setValue(val_power2); // 1week_ago 
  sheet6Data.getRange(2, 3).setValue(val_power2); // 1month_ago 
  sheet7Data.getRange(2, 3).setValue(val_power2); // 1year_ago 

}
